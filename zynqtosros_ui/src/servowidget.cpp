// Copyright 2018 Anders Bøgild (andb@mmmi.sdu.dk)
// Training Technology Lab
// SDU Health Informatics and Technology, The Maersk Mc-Kinney Moller Institute.

#include "servowidget.h"
#include "ui_servowidget.h" //auto generated header file from uic

#include <math.h>

ServoWidget::ServoWidget(QWidget *parent):
  QWidget(parent),
  ui(new Ui::ServoWidget)
{
    ui->setupUi(this);

    connect(ui->sldrServo, SIGNAL(sliderMoved(int)), this, SLOT(onSliderMovedServo(int)));
    connect(ui->pbSendServoCmd, SIGNAL(clicked()), this, SLOT(onSendServoCmdClicked()));
    connect(ui->cbContUpdServo, SIGNAL(toggled(bool)), this, SLOT(onContUpdServoToggled(bool)));
}

ServoWidget::~ServoWidget()
{
    delete ui;
}

long ServoWidget::mapSliderToServoRange(long sld, long servo_max, long servo_min)
{
    return sld * (servo_max - servo_min)/(100);
}

void ServoWidget::onSendServoCmdClicked()
{
  //ROS_INFO("Click: %i", ui->sldrServo1->value());
  //callServoCommand(1, 1, ui->sldrServo1->value());
  emit(ServoCommand(ui->sbServoId->value(), ui->sbServoRegOffset->value(),
    (int)mapSliderToServoRange(ui->sldrServo->value(), 55000, 15000))
  );
}

void ServoWidget::onSliderMovedServo(int val)
{
  if(ui->cbContUpdServo->isChecked())
  {
    int servo_value = (int)mapSliderToServoRange(val, 55000, 15000);
    emit(ServoCommand(ui->sbServoId->value(), ui->sbServoRegOffset->value(), servo_value));
  }
}

void ServoWidget::onContUpdServoToggled(bool active)
{
  ui->pbSendServoCmd->setEnabled(!active);
  emit(ContinousUpdateToggled(active));
}

// --- external signal handling ---
void ServoWidget::onDemoTimeUpdated(float t)
{
  ui->sldrServo->setValue(50+(int)(sin(t+phase)*50));
  onSliderMovedServo(ui->sldrServo->value());
}

void ServoWidget::onDemoModeToggled(bool active)
{
  if(active)
  {
    phase = -M_PI_2+(ui->sldrServo->value())/100.0*1.0*M_PI;
  }
}
