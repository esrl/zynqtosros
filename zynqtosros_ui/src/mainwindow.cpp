// Copyright 2018 Anders Bøgild (andb@mmmi.sdu.dk)
// Training Technology Lab
// SDU Health Informatics and Technology, The Maersk Mc-Kinney Moller Institute.

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QApplication>
#include <QTimer>
#include <QString>
//#include <QList>
//#include <QTextStream>
#include <ros/ros.h>
#include <zynqtosros_msgs/servo_command.h>
#include <zynqtosros_msgs/servo_command_msg.h>
#include <zynqtosros_msgs/servo_command_msga.h>
#include <zynqtosros_msgs/controller_command.h>
#include <zynqtosros_msgs/servo_status.h>
#include <zynqtosros_msgs/sensor_command_msg.h>
#include <zynqtosros_msgs/bitflip_command_srv.h>
#include <zynqtosros_msgs/bitflip_status_msg.h>

#include <math.h>


MainWindow::MainWindow(int argc, char **argv, QWidget *parent, QApplication *app) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    app(app),
    cout(stdout)
{

    ros::init(argc, argv, "zynqtosros_ui_node");
    ros::NodeHandle n;
    ros::start(); // explicitly needed since our nodehandle is going out of scope.

    //Setup UI and list widget
    ui->setupUi(this);

    //servo = new ServoWidget(this);
    //servo2 = new ServoWidget(this);
    ui->servoLayout->addStretch();

    /*connect(ui->sldrServo1, SIGNAL(sliderMoved(int)), this, SLOT(onSliderMovedServo1(int)));
    connect(ui->sldrServo2, SIGNAL(sliderMoved(int)), this, SLOT(onSliderMovedServo2(int)));
    connect(ui->sldrServo3, SIGNAL(sliderMoved(int)), this, SLOT(onSliderMovedServo3(int)));
    connect(ui->sldrServo4, SIGNAL(sliderMoved(int)), this, SLOT(onSliderMovedServo4(int)));

    connect(ui->pbSendServoCmd1, SIGNAL(clicked()), this, SLOT(onSendServoCmdClicked1()));
    connect(ui->pbSendServoCmd2, SIGNAL(clicked()), this, SLOT(onSendServoCmdClicked2()));
    connect(ui->pbSendServoCmd3, SIGNAL(clicked()), this, SLOT(onSendServoCmdClicked3()));
    connect(ui->pbSendServoCmd4, SIGNAL(clicked()), this, SLOT(onSendServoCmdClicked4()));

    connect(ui->cbContUpdServo1, SIGNAL(toggled(bool)), this, SLOT(onContUpdServoToggled1(bool)));
    connect(ui->cbContUpdServo2, SIGNAL(toggled(bool)), this, SLOT(onContUpdServoToggled2(bool)));
    connect(ui->cbContUpdServo3, SIGNAL(toggled(bool)), this, SLOT(onContUpdServoToggled3(bool)));
    connect(ui->cbContUpdServo4, SIGNAL(toggled(bool)), this, SLOT(onContUpdServoToggled4(bool)));
*/
    connect(ui->cbDemoMode, SIGNAL(toggled(bool)), this, SLOT(onDemoModeToggled(bool)));
    connect(ui->sbDemoModeInterval, SIGNAL(valueChanged(int)), this, SLOT(onDemoModeIntervalChanged(int)));

    connect(ui->pbSendSensorCmd, SIGNAL(clicked()), this, SLOT(onSendSensorCmdClicked()));
    connect(ui->cbContUpdSensor, SIGNAL(toggled(bool)), this, SLOT(onContUpdSensorToggled(bool)));
    connect(ui->sbSensorReadInterval, SIGNAL(valueChanged(int)), this, SLOT(onSensorReadIntervalChanged(int)));

    connect(ui->sbNumServos, SIGNAL(valueChanged(int)), this, SLOT(onNumServosChanged(int)));
    onNumServosChanged(ui->sbNumServos->value()); //initiate servo widgets

    connect(ui->pbBitFlipEnable, SIGNAL(clicked()), this, SLOT(onBitFlipXableClicked()));
    connect(ui->pbBitFlipDisable, SIGNAL(clicked()), this, SLOT(onBitFlipXableClicked()));

/*
    ui->listWidget->setSelectionMode(QAbstractItemView::MultiSelection);
    ui->listWidget->setSortingEnabled(true);
    connect(ui->listWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(onItemClicked(QListWidgetItem*)));
    connect(ui->leRosBagFileNamePrefix, SIGNAL(editingFinished()), this, SLOT(onLineEditFinished()));

    connect(ui->leRosBagFileLocation, SIGNAL(editingFinished()), this, SLOT(onLineEditFinished()));
    //Configure buttons and time label
    setUiButtonsModeInitial();
    resetUiTimeLabel();

    //Init UI line edit widgets with current parameters
    updateUiPathSave();
    updateUiFilePrefix();
*/
    servo_command_pub = n.advertise<zynqtosros_msgs::servo_command_msg>(n.param(std::string("servo_command_topic"), std::string("/zynqtosros_interface/servo_command_topic")),1);
    servo_command_arr_pub = n.advertise<zynqtosros_msgs::servo_command_msga>(n.param(std::string("servo_command_arr_topic"), std::string("/zynqtosros_interface/servo_command_arr_topic")),1);

    sensor_command_pub = n.advertise<zynqtosros_msgs::sensor_command_msg>(n.param(std::string("sensor_command_topic"), std::string("/zynqtosros_interface/sensor_command_topic")),1);
    sensor_status_sub = n.subscribe(n.param(std::string("sensor_status_topic"), std::string("/zynqtosros_interface/sensor_status_topic")), 1, &MainWindow::sensor_status_msg_cb, this);

    bitflip_status_sub = n.subscribe(n.param(std::string("bitflip_status_topic"), std::string("/zynqtosros_interface/bitflip_status_topic")), 1, &MainWindow::bitflip_status_msg_cb, this);

    //Setup and start GUI timer. This will also spin ROS.
    pTimer = new QTimer(this);
    connect(pTimer, SIGNAL(timeout()), this, SLOT(gui_update()));
    pTimer->start(1000);
}

MainWindow::~MainWindow()
{
    delete ui;
}

/*long MainWindow::mapSliderToServoRange(long sld, long servo_max, long servo_min)
{
    return sld * (servo_max - servo_min)/(100);
}
*/

void MainWindow::gui_update()
{
    if(ros::ok())
    {
        ros::spinOnce();
    }

    //when ros::ok() is no longer true then ROS is done, and we can safely stop the Qt application.
    else
    {
        pTimer->stop();
        app->quit();
    }
}

void MainWindow::onBitFlipXableClicked()
{
    callBitFlipService();
}

void MainWindow::onNumServosChanged(int num_servos)
{

  //ROS_INFO("Changed: num_servos=%i layout count=%i", num_servos,ui->servoLayout->count());
  int delta_servos = num_servos - (ui->servoLayout->count()-1);//vecPServoWidget.size();

  //Adding more servo widgets
  if( delta_servos > 0 )
  {
    //Remove stretch at bottom
    ui->servoLayout->takeAt(ui->servoLayout->count()-1);

    //Add widgets
    while( delta_servos != 0 )
    {
        ServoWidget *pservo = new ServoWidget(this);
        connect(this, SIGNAL(DemoTimeUpdated(float)), pservo, SLOT(onDemoTimeUpdated(float)));
        connect(this, SIGNAL(DemoModeToggled(bool)), pservo, SLOT(onDemoModeToggled(bool)));
        connect(pservo, SIGNAL(ContinousUpdateToggled(bool)), this, SLOT(onContUpdServoToggled(bool)));
        connect(pservo,
          SIGNAL(ServoCommand(unsigned, unsigned, unsigned)),
          this,
          SLOT(onServoCommandReceived(unsigned, unsigned, unsigned))
        );
        //connect(pservo,SIGNAL(ServoCommando()), this, SLOT(onServoCommandoReceived()));
        ui->servoLayout->addWidget(pservo);
        vecPServoWidget.push_back(pservo);
        //ROS_INFO("+widget num_servos=%i delta_servos=%i qvec size=%i layout count=%i", num_servos, delta_servos, vecPServoWidget.size(),ui->servoLayout->count());
        delta_servos--;
    }

    //Re insert stretch at bottom
    ui->servoLayout->addStretch();

  }

  //Removing servo widgets
  else if( delta_servos < 0)
  {
    //We can only remove if there is at least one servo (remember to count the stretcher item)
    if(ui->servoLayout->count() > 1)
    {
      while(delta_servos != 0 )
      {
        //ServoWidget *pservo = (ServoWidget*)(ui->servoLayout->takeAt(ui->servoLayout->count()-2));
        //delete pservo;
        ui->servoLayout->removeWidget(vecPServoWidget.last());
        disconnect(this, SIGNAL(DemoTimeUpdated(float)), vecPServoWidget.last(), SLOT(onDemoTimeUpdated(float)));
        disconnect(this, SIGNAL(DemoModeToggled(bool)), vecPServoWidget.last(), SLOT(onDemoModeToggled(bool)));
        disconnect(vecPServoWidget.last(), SIGNAL(ContinousUpdateToggled(bool)), this, SLOT(onContUpdServoToggled(bool)));
        disconnect(vecPServoWidget.last(),
          SIGNAL(ServoCommand(unsigned, unsigned, unsigned)),
          this,
          SLOT(onServoCommandReceived(unsigned, unsigned, unsigned))
        );
        //disconnect(vecPServoWidget.last(), SIGNAL(ServoCommando()), this, SLOT(onServoCommandoReceived()));
        delete vecPServoWidget.last();
        vecPServoWidget.pop_back(); //remove last element
        //ROS_INFO("-widget num_servos=%i delta_servos=%i qvec size=%i layout count=%i", num_servos, delta_servos, vecPServoWidget.size(),ui->servoLayout->count());
        delta_servos++;
      }
    }
  }


  /*QVectorIterator<ServoWidget*> it(vecPServoWidget);
  while(it.hasNext())
  {
    ui->servoLayout->addWidget(it.next());
  }
  ui->servoLayout->addStretch(); //push servo widgets to top of layout
  */
}

void MainWindow::onSendSensorCmdClicked()
{
  publishSensorCommand(ui->sbSensorId->value(), ui->sbSensorRegOffset->value());
}
/*
void MainWindow::onSendServoCmdClicked1()
{
  //ROS_INFO("Click: %i", ui->sldrServo1->value());
  //callServoCommand(1, 1, ui->sldrServo1->value());
  publishServoCommand(ui->sbServoId1->value(), ui->sbServoRegOffset1->value(),
    (int)mapSliderToServoRange(ui->sldrServo1->value(), 55000, 15000)
  );
}
void MainWindow::onSendServoCmdClicked2()
{
  publishServoCommand(ui->sbServoId2->value(), ui->sbServoRegOffset2->value(),
    (int)mapSliderToServoRange(ui->sldrServo2->value(), 55000, 15000)
  );
}
void MainWindow::onSendServoCmdClicked3()
{
  publishServoCommand(ui->sbServoId3->value(), ui->sbServoRegOffset3->value(),
    (int)mapSliderToServoRange(ui->sldrServo3->value(), 55000, 15000)
  );
}

void MainWindow::onSendServoCmdClicked4()
{
  publishServoCommand(ui->sbServoId4->value(), ui->sbServoRegOffset4->value(),
    (int)mapSliderToServoRange(ui->sldrServo4->value(), 55000, 15000)
  );
}

void MainWindow::onSliderMovedServo1(int val)
{
  if(ui->cbContUpdServo1->isChecked())
  {
      //ROS_INFO("Slide1: %i", val);

    int servo_value = (int)mapSliderToServoRange(val, 55000, 15000);
    //servo_value += 2162688; //servo id 0x21 on byte 3
    //servo_value += 65536; //servo id 0x1 on byte 3
    publishServoCommand(ui->sbServoId1->value(), ui->sbServoRegOffset1->value(), servo_value);
  }
}
void MainWindow::onSliderMovedServo2(int val)
{
  if(ui->cbContUpdServo2->isChecked())
  {
      //ROS_INFO("Slide2: %i", val);
    int servo_value = (int)mapSliderToServoRange(val, 55000, 15000);
    //servo_value += 2228224; //servo id 0x22 on byte 3
    //servo_value += 131072; //servo id 0x2 on byte 3
    publishServoCommand(ui->sbServoId2->value(), ui->sbServoRegOffset2->value(), servo_value);

  }
}
void MainWindow::onSliderMovedServo3(int val)
{
  if(ui->cbContUpdServo3->isChecked())
  {
      //ROS_INFO("Slide3: %i", val);
    int servo_value = (int)mapSliderToServoRange(val, 55000, 15000);
    //servo_value += 3211264; //servo id 0x31 on byte 3
    //servo_value += 196608; //servo id 0x3 on byte 3
    publishServoCommand(ui->sbServoId3->value(), ui->sbServoRegOffset3->value(), servo_value);

  }
}
void MainWindow::onSliderMovedServo4(int val)
{
  if(ui->cbContUpdServo4->isChecked())
  {
      //ROS_INFO("Slide4: %i", val);
    int servo_value = (int)mapSliderToServoRange(val, 55000, 15000);
    //servo_value += 3276800; //servo id 0x32 on byte 3
    publishServoCommand(ui->sbServoId4->value(), ui->sbServoRegOffset4->value(), servo_value);

  }
}
*/
void MainWindow::onDemoModeToggled(bool active)
{
  ROS_INFO("Demo Mode: %s", active?"on":"off");

  emit(DemoModeToggled(active));

  if(active)
  {
    pDemoTimer = new QTimer(this);
    connect(pDemoTimer, SIGNAL(timeout()), this, SLOT(demo_timer_update()));
/*
    //get current phase
    phase_s1 = -M_PI_2+(ui->sldrServo1->value())/100.0*1.0*M_PI;
    phase_s2 = -M_PI_2+(ui->sldrServo2->value())/100.0*1.0*M_PI;
    phase_s3 = -M_PI_2+(ui->sldrServo3->value())/100.0*1.0*M_PI;
    phase_s4 = -M_PI_2+(ui->sldrServo4->value())/100.0*1.0*M_PI;
    ROS_INFO("Phase S1: %f", phase_s1);
    ROS_INFO("Phase S2: %f", phase_s2);
    ROS_INFO("Phase S3: %f", phase_s3);
    ROS_INFO("Phase S4: %f", phase_s4);
    */
    pDemoTimer->start(ui->sbDemoModeInterval->value());

  }
  else
  {
    pDemoTimer->stop();
    disconnect(pDemoTimer, SIGNAL(timeout()), this, SLOT(demo_timer_update()));
    delete pDemoTimer;
    pDemoTimer=nullptr;
    t_demo=0;
  }
}

void MainWindow::onDemoModeIntervalChanged(int value)
{
  if(pDemoTimer!=nullptr){
    pDemoTimer->setInterval(value);
  }
}

void MainWindow::onSensorReadIntervalChanged(int value)
{
  if(pSensorReadTimer!=nullptr){
    pSensorReadTimer->setInterval(value);
  }
}

void MainWindow::demo_timer_update()
{
    //ROS_INFO("Demo time!");

/*//Round Robin
  static int idx=0;
    switch(idx)
    {
    case 0:
	ui->sldrServo1->setValue(50+(int)(sin(t_demo+phase_s1)*50));
	onSliderMovedServo1(ui->sldrServo1->value());
	break;
    case 1:
	ui->sldrServo2->setValue(50+(int)(sin(t_demo+phase_s2)*50));
	onSliderMovedServo2(ui->sldrServo2->value());
	break;
    case 2:
	ui->sldrServo3->setValue(50+(int)(sin(t_demo+phase_s3)*50));
	onSliderMovedServo3(ui->sldrServo3->value());
	break;
    case 3:
	ui->sldrServo4->setValue(50+(int)(sin(t_demo+phase_s4)*50));
	onSliderMovedServo4(ui->sldrServo4->value());
	break;
    }

    if(++idx>3) idx =0;
*/
/*    ui->sldrServo1->setValue(50+(int)(sin(t_demo+phase_s1)*50));
    onSliderMovedServo1(ui->sldrServo1->value());

    ui->sldrServo2->setValue(50+(int)(sin(t_demo+phase_s2)*50));
    onSliderMovedServo2(ui->sldrServo2->value());

    ui->sldrServo3->setValue(50+(int)(sin(t_demo+phase_s3)*50));
    onSliderMovedServo3(ui->sldrServo3->value());

    ui->sldrServo4->setValue(50+(int)(sin(t_demo+phase_s4)*50));
    onSliderMovedServo4(ui->sldrServo4->value());
*/
    emit(DemoTimeUpdated(t_demo));

    t_demo+=0.02;
}

void MainWindow::sensor_timer_update()
{
  onSendSensorCmdClicked();
}

// --- external signals handling ---
void MainWindow::onContUpdServoToggled(bool active)
{
  ROS_INFO("Widget Toggled: %s", active?"on":"off");
  if(active)
    num_servos++;
  else
    num_servos--;
}
/*
void MainWindow::onContUpdServoToggled1(bool active)
{
  //ROS_INFO("Toggled: %s", active?"on":"off");
  if(active)
  {
    ui->pbSendServoCmd1->setEnabled(false);
    num_servos++;
  }
  else
  {
    ui->pbSendServoCmd1->setEnabled(true);
    num_servos--;
  }
}

void MainWindow::onContUpdServoToggled2(bool active)
{
  if(active)
  {
    ui->pbSendServoCmd2->setEnabled(false);
    num_servos++;
  }
  else
  {
    ui->pbSendServoCmd2->setEnabled(true);
    num_servos--;
  }
}
void MainWindow::onContUpdServoToggled3(bool active)
{
  if(active)
  {
    ui->pbSendServoCmd3->setEnabled(false);
    num_servos++;
  }
  else
  {
    ui->pbSendServoCmd3->setEnabled(true);
    num_servos--;
  }
}
void MainWindow::onContUpdServoToggled4(bool active)
{
  if(active)
  {
    ui->pbSendServoCmd4->setEnabled(false);
    num_servos++;
  }
  else
  {
    ui->pbSendServoCmd4->setEnabled(true);
    num_servos--;
  }
}
*/

//Calling a ROS service
void MainWindow::callServoCommand(int id, int key, int value)
{
    zynqtosros_msgs::servo_command srv_call;

    srv_call.request.servo_id=id;
    srv_call.request.key=key;
    srv_call.request.value=value;

    ros::service::call("/zynqtosros_interface/servo_command", srv_call);

    if(!srv_call.response.success)
      ROS_WARN_STREAM("error: " << srv_call.response.error);
}

//Calling service to enable bit flip test on intrface
void MainWindow::callBitFlipService()
{
    ROS_INFO_STREAM("MainWindow::callBitFlipService()");
    zynqtosros_msgs::bitflip_command_srv srv_call;
    srv_call.request.servo_id = ui->sbBitFlipMotorId->value();

    ros::service::call("/zynqtosros_interface/bitflip_command", srv_call);

    if(srv_call.response.enabled)
    {
      ui->pbBitFlipDisable->setEnabled(true);
      ui->pbBitFlipEnable->setEnabled(false);
      ui->sbBitFlipMotorId->setEnabled(false);
    }
    else
    {
      ui->pbBitFlipDisable->setEnabled(false);
      ui->pbBitFlipEnable->setEnabled(true);
      ui->sbBitFlipMotorId->setEnabled(true);
    }
}

//Qt sigal from servo widgets
void MainWindow::onServoCommandReceived(unsigned id, unsigned addr, unsigned value)
{
  publishServoCommand(id, addr, value);
}

/*void MainWindow::onServoCommandoReceived()
{
  ROS_INFO("BLAH");
}*/

//Publishing a ROS message
void MainWindow::publishServoCommand(unsigned int id, unsigned int addr, unsigned int value)
{

  static std::vector<unsigned int> vec_id = {};
  static std::vector<unsigned int> vec_addr = {};
  static std::vector<unsigned int> vec_value = {};

  if(ui->cbDemoMode->isChecked())
  {

      vec_id.push_back(id);
      vec_addr.push_back(addr);
      vec_value.push_back(value);

      if (vec_id.size()==num_servos)
      {
	  publishServoCommandArr(num_servos, vec_id, vec_addr, vec_value);
	  vec_id.clear();
	  vec_addr.clear();
	  vec_value.clear();
      }
  }
  else
  {
      zynqtosros_msgs::servo_command_msg msg_pub;

      msg_pub.servo_id=id;
      msg_pub.reg_offset=addr;
      msg_pub.value=value;

      servo_command_pub.publish(msg_pub);
  }

}

void MainWindow::publishServoCommandArr(unsigned int num, std::vector<unsigned int> id, std::vector<unsigned int> addr, std::vector<unsigned int> value)
{
  zynqtosros_msgs::servo_command_msga msga_pub;

  msga_pub.num_servos = num;

  msga_pub.servo_id=id;
  msga_pub.reg_offset=addr;
  msga_pub.value=value;

  servo_command_arr_pub.publish(msga_pub);

}

void MainWindow::publishSensorCommand(unsigned int id, unsigned int addr)
{
  zynqtosros_msgs::sensor_command_msg msg_pub;

  msg_pub.sensor_id=id;
  msg_pub.reg_offset=addr;

  sensor_command_pub.publish(msg_pub);

}

void MainWindow::sensor_status_msg_cb(const zynqtosros_msgs::sensor_status_msg::ConstPtr &msg)
{
  ui->lblSensorValue->setText(QString("%1").arg(msg->value,4,16));
}

void MainWindow::onContUpdSensorToggled(bool active)
{
  if(active)
  {

    ui->pbSendSensorCmd->setEnabled(false);
    ui->sbSensorReadInterval->setEnabled(true);

    pSensorReadTimer = new QTimer(this);
    connect(pSensorReadTimer, SIGNAL(timeout()), this, SLOT(sensor_timer_update()));
    pSensorReadTimer->start(ui->sbSensorReadInterval->value());
  }
  else
  {
    ui->sbSensorReadInterval->setEnabled(false);
    ui->pbSendSensorCmd->setEnabled(true);

    pSensorReadTimer->stop();
    disconnect(pSensorReadTimer, SIGNAL(timeout()), this, SLOT(sensor_timer_update()));
    delete pSensorReadTimer;
    pSensorReadTimer=nullptr;
  }
}

void MainWindow::bitflip_status_msg_cb(const zynqtosros_msgs::bitflip_status_msg::ConstPtr &msg)
{
  ui->lblBitFlipCount->setText(QString("%1").arg(msg->count,5,10));
}
