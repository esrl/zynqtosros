// Copyright 2018 Anders Bøgild (andb@mmmi.sdu.dk)
// Training Technology Lab
// SDU Health Informatics and Technology, The Maersk Mc-Kinney Moller Institute.

#include <QObject>
#include <QWidget>

namespace Ui {
    class ServoWidget;
}

class ServoWidget : public QWidget
{
  Q_OBJECT

public:
    ServoWidget(QWidget *parent = 0);
    ~ServoWidget();

    void setPhase(float phase);

signals:
    void ContinousUpdateToggled(bool);
    void ServoCommand(unsigned id, unsigned addr, unsigned value);

public slots:
    void onDemoTimeUpdated(float);
    void onDemoModeToggled(bool);

private slots:
    void onSendServoCmdClicked();
    void onSliderMovedServo(int);
    void onContUpdServoToggled(bool);

private:
    long mapSliderToServoRange(long sld, long servo_max, long servo_min);
    //void publishServoCommand(unsigned int id, unsigned int addr, unsigned int value);

    Ui::ServoWidget *ui;
    float phase = 0.0;
    //float t_demo = 0.0;

};
