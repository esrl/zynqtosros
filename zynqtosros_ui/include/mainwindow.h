// Copyright 2018 Anders Bøgild (andb@mmmi.sdu.dk)
// Training Technology Lab
// SDU Health Informatics and Technology, The Maersk Mc-Kinney Moller Institute.

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QApplication>
#include <QTextStream>
#include <QTimer>
#include <math.h>
#include <vector>
#include <ros/ros.h>
#include <zynqtosros_msgs/sensor_status_msg.h>
#include <zynqtosros_msgs/bitflip_status_msg.h>
#include <servowidget.h>

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(int argc, char** argv, QWidget *parent = 0, QApplication *app = nullptr);
    ~MainWindow();

private slots:
    void gui_update();
/*
    void onSendServoCmdClicked1();
    void onSendServoCmdClicked2();
    void onSendServoCmdClicked3();
    void onSendServoCmdClicked4();

    void onSliderMovedServo1(int);
    void onSliderMovedServo2(int);
    void onSliderMovedServo3(int);
    void onSliderMovedServo4(int);

    void onContUpdServoToggled1(bool);
    void onContUpdServoToggled2(bool);
    void onContUpdServoToggled3(bool);
    void onContUpdServoToggled4(bool);
*/

    void onDemoModeToggled(bool);
    void onDemoModeIntervalChanged(int);

    void demo_timer_update();
    void sensor_timer_update();

    void onSendSensorCmdClicked();
    void onContUpdSensorToggled(bool);
    void onSensorReadIntervalChanged(int);

    void onBitFlipXableClicked();

    void onNumServosChanged(int);
    void onContUpdServoToggled(bool);
    void onServoCommandReceived(unsigned id, unsigned addr, unsigned value);
    //void onServoCommandoReceived(int);

//    void timer_update();
//    void on_btnRecord_clicked();
//    void on_btnRecordStop_clicked();
//    void on_btnGetTopics_clicked();
//    void onItemClicked(QListWidgetItem*);
//    void onLineEditFinished();
signals:
    void DemoTimeUpdated(float);
    void DemoModeToggled(bool);

private:
    Ui::MainWindow *ui;
    QTextStream cout;
    QApplication *app;
    QTimer *pTimer;
    QTimer *pDemoTimer=nullptr;
    QTimer *pSensorReadTimer=nullptr;
    float t_demo=0.0;
    float phase_s1=0.0, phase_s2=0.0, phase_s3=0.0, phase_s4=0.0;
    unsigned int num_servos=0;
//    QVector<QListWidgetItem*> *pQVecClickedItems;
    QVector<ServoWidget*> vecPServoWidget;
    //ServoWidget *servo;
    //ServoWidget *servo2;

//    unsigned int recordTimeSec;


    ros::Publisher servo_command_pub;
    ros::Publisher servo_command_arr_pub;

    ros::Publisher sensor_command_pub;
    ros::Subscriber sensor_status_sub;

    void callServoCommand(int id, int key, int value);
    void publishServoCommand(unsigned int id, unsigned int addr, unsigned int value);
    void publishServoCommandArr(unsigned int num, std::vector<unsigned int> id, std::vector<unsigned int> addr, std::vector<unsigned int> value);

    void callBitFlipService();
    ros::Subscriber bitflip_status_sub;
    //long mapSliderToServoRange(long sld, long servo_max, long servo_min);

    void publishSensorCommand(unsigned int id, unsigned int addr);
    void sensor_status_msg_cb(const zynqtosros_msgs::sensor_status_msg::ConstPtr &msg);

    void bitflip_status_msg_cb(const zynqtosros_msgs::bitflip_status_msg::ConstPtr &msg);


//    void sendCommandRecord();
//    void callServiceCmd(std::string str_cmd);

//    void updateUiPathSave();
//    void updateUiFilePrefix();
//    void updateUiTopics();

//    void updateParameterPathSave();
//    void updateParameterFilePrefix();
//    void updateParameterTopics();

//    void setUiButtonsModeRecording();
//    void setUiButtonsModeStopped();
//    void setUiButtonsModeInitial();

//    void resetUiTimeLabel();
//    void setUiTimeLabel(int time_secs);

//    bool isRecording();


};

#endif // MAINWINDOW_H
