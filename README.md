# Installing Ubuntu 16.04 root file system on PetaLinux running on the Zynq
**Performed with the SDCard, containing a bootable PetaLinux (with partitions BOOT and ROOT_FS), mounted on linux PC running Ubuntu 16.04**.  
**If you have just completed the ["How to build the ZYBO ROS hardware framework"](https://gitlab.com/esrl/tutorials/wikis/How-to-build-the-ZYBO-ROS-hardware-framework) tutorial, you have already completed this step**.

Get ubuntu root fs from https://rcn-ee.com/rootfs/eewiki/minfs/ubuntu-16.04.4-minimal-armhf-2018-03-26.tar.xz

Format the SD card partition for root fs:

    sudo mkfs.ext4 -L “ROOT_FS” /dev/<partition name. E.g.: sdb2>

Remount ROOT_FS partition, un-archive .xz file to get the .tar and then:

    sudo tar xf armhf-rootfs-ubuntu-xenial.tar --strip-components=1 -C /media/<user>/ROOT_FS/

Reboot and enjoy Ubuntu booting on your Zynq, and just apt-get install what ever you want from the Ubuntu repository.

# Preparing your local machine (running Ubuntu 16.04).
**Performed on the linux PC, connected to the same local network as the Zynq board running Ubuntu 16.04.**

We want to setup password-less public/private key ssh login to the remote zynq board. This allows the ROS master to launch nodes remotely.
On your local linux machine, make sure known hosts_file is clear (at least for entries regarding the remote zynq board)

    nano ~/.ssh/known_hosts

ROS is using an old algorithm for host keys and therefore we need to force this 
at first connection. Connect and login to each.

    ssh -oHostKeyAlgorithms='ssh-rsa' ubuntu@arm.local

Create a public/private keypair, if not already done.
**Note: The keygen command should be run only once on your local machine.** Your 
previous private key will be overwritten by ssh-keygen.

    ssh-keygen -t rsa 

Press ENTER to every question/field.

Copy the public part of the keypair to the remote Zynq board running Ubuntu 16.04

    ssh-copy-id ubuntu@arm.local

Setup the ROS host name environment by executing the next two lines from the terminal. This will make sure that the settings take effect every time a new bash terminal session is started on your local machine.

    echo "export ROS_HOSTNAME=$HOSTNAME.local" >> ~/.bashrc
    echo "export ROS_MASTER_URI=http://$HOSTNAME.local:11311" >> ~/.bashrc

# Install ROS on Ubuntu 16.04 running on the Zynq and Linux machine
**Performed on the Zynq board running Ubuntu 16.04 (with working internet connection)**  
**Performed on the linux PC running Ubuntu 16.04**  
**All steps in this section should be completed on both the Zynq board and the linux PC**  
**If a router is not available, follow this [guide](https://gitlab.com/esrl/linux_zynq/wikis/Connecting%20to%20the%20internet%20via%20linux%20pc) to share your PC's internet connection with the Zynq board**   

Install the mDNS package, allowing ROS to reference it on the dotlocal domain (arm.local in this case).

    sudo apt-get install libnss-mdns

Follow the official ROS installation instructions for installing ROS "Kinetic Kame" on Ubuntu 16.04 "Xenial": 
http://wiki.ros.org/kinetic/Installation/Ubuntu

After following the guide, we need to do a few more steps to setup and initialise 
the catkin workspace.

    mkdir -p ~/catkin_ws/src
    cd ~/catkin_ws
    catkin_make
    
    echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
    echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
    
Restart the Zybo board for everything to take effect:

    sudo shutdown -r now

Log in to the Zybo board again. This time it should be possible to do it using the .local domain (password is "temppwd"):

    ssh arm@ubuntu.local

You can check that the ROS intallation is setup correctly by inspecting the
environmental variable ROS_PACKAGE_PATH.

    echo $ROS_PACKAGE_PATH
    
The output should be "/home/youruser/catkin_ws/src:/opt/ros/kinetic/share".
    



# Preparing the remote zynq board (running Ubuntu 16.04)
**Performed Zynq board running Ubuntu 16.04.**

Setup the ROS host name environment by executing the next two lines from the terminal. This will make sure that the settings take effect every time a new bash terminal session is started on the Zybo.

    echo "export ROS_HOSTNAME=$HOSTNAME.local" >> ~/.bashrc
    echo "export ROS_MASTER_URI=http://localhost:11311" >> ~/.bashrc

Create a file **ros_env.sh** in the home directory of the ubuntu user (home directory is just ~)

    nano ros_env.sh

Copy the text of **ros_env.sh** from this repository to the newly created file on the Zynq board. Save and make executable for the current user. 

    chmod u+x ros_env.sh

Make sure that the user (default *ubuntu*) on the Zynq board is member of the group *dialout*

    sudo adduser ubuntu dialout

Restart the zybo board for group changes to take effect.

    sudo shutdown -r now
    
# Install kernel headers for PetaLinux kernel on remote zynq board
**Performed Zynq board running Ubuntu 16.04.**

Determine kernel version on target:

    uname -r 
    
Petalinux 2017.4 responds with "4.9.0-xilinx-v2017.4", which can be downloaded from Xilinx linux-xlnx project on Github. The specific version has been tagged "xilinx-v2017.4"

    wget --no-check-certificat https://github.com/Xilinx/linux-xlnx/archive/xilinx-v2017.4.zip
    unzip xilinx-v2017.4.zip

Configure kernel

    cd linux-xlnx-xilinx-v2017.4
    make ARCH=arm xilinx_zynq_defconfig

Prepare kernel for module development without compiling kernel itself:

    make prepare
    make modules_prepare

**The below steps are only if you are compiling on the linux PC instead of the Zynq board**      
If you prefer to compile the kernel headers on the linux PC instead of locally on the Zynq board, it is necessary to setup the cross compile environment by issuing the following commands from the bash terminal:

    export CROSS_COMPILE=arm-linux-gnueabihf-
    source <Xilinx installation directory>/Vivado/<version>/settings64.sh    
    
Follow the steps from wget through make. When compiling on the linux machine, run the following after the "make ARCH=arm xilinx_zynq_defconfig" command:

    make ARCH=arm menuconfig

Continue with the make prepare steps and then transfer files to the Zynq board.    

# Fetch and compile ZynqTOSROS on the remote Zynq board
**Performed Zynq board running Ubuntu 16.04, with ROS Kinetic installed (following the guide above)**

Fetch the repository from git and place it in your ROS catkin workspace.

    cd ~/catkin_ws/src
    git clone git@gitlab.esrl.dk:SDU-embedded/zynq/zynqtosros.git

Since the Qt userinterface part of the project is not intended to run on the Zynq, we need to delete that part og the projekt

    rm -rf zynqtosros/zynqtosros_ui

Now the ROS project is ready to be compiled

    cd ~/catkin_ws && catkin_make

# Fetch and compile ZynqTOSROS on the linux PC
**Performed on the linux PC running Ubuntu 16.04, with ROS Kinetic installed (following the guide above)**

Fetch the repository from git and place it in your ROS catkin workspace.

    cd ~/catkin_ws/src
    git clone git@gitlab.esrl.dk:SDU-embedded/zynq/zynqtosros.git

Now the ROS project is ready to be compiled

    cd ~/catkin_ws && catkin_make

# Compile the linux kernel character device driver on the remote Zynq board
**Performed Zynq board running Ubuntu 16.04.**

The kerneldriver needs to be compiled seperately, as it is not a ROS component. The source code for it is, however, placed in **zynqtosros/zynqtosros_interface/module**.

    cd ~/catkin_ws/src/zynqtosros/zynqtosros_interface/module
    make

If you need to change the path to the location of the kernel headers downloaded and prepared in the previous step, you can edit **Makefile** and change KDIR= acordingly.

To load the kernel device driver as it is used in this projekt, simply run the bash scrip load.sh

    ./load.sh

You shoud check which major number the device driver is assigned when loading. The major number is what ties the zynqtos kernel device driver to the device files created in **load.sh**. They need to match. 

    dmesg

The **load.sh** script assumes major number 244 is assigned to the driver by the linux kernel, but that is not gueranteed. Check output from **dmesg** and and change **load.sh** accordingly.

To unload the driver type:

    sudo rmmod zynqtosros

If you might need to verify that the baseaddress of your PL is correct and change accordingly.

For debugging, *devmem2* is a useful tool for reading and writing content of memorylocations in 8, 16 and 32 bit width. It is part of the Ubuntu Xenial repository:

    sudo apt install devmem2

To see how it can be used, take a look in **bitflip.sh** 

# Test framework  
Connect to the Zynq board

    ssh ubuntu@arm.local
    
Load kernel device driver (if you have just completed the guide, this is probably already loaded):

    cd ~/catkin_ws/src/zynqtosros/zynqtosros_interface/module
    ./load.sh
    
**On the linux PC**  
In a new terminal window, open the GUI to send commands to the Zynq board

    roslaunch zynqtosros_ui start_remote_zynq.launch
    
If you are only using a single Zynq board, use motor IDs 0x01-0x10 to test motors and sensor IDs 0x1-0x4 to test sensor input.     
