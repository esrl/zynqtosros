// Copyright 2018 Anders Bøgild (andb@mmmi.sdu.dk)
// Training Technology Lab
// SDU Health Informatics and Technology, The Maersk Mc-Kinney Moller Institute.

#ifndef ZYNQTOSROS_INTERFACE_HPP
#define ZYNQTOSROS_INTERFACE_HPP

#include <ros/ros.h>
#include <string>

#include <zynqtosros_msgs/servo_status.h>
#include <zynqtosros_msgs/servo_command.h>
#include <zynqtosros_msgs/servo_command_msg.h>
#include <zynqtosros_msgs/servo_command_msga.h>
#include <zynqtosros_msgs/sensor_command_msg.h>
#include <zynqtosros_msgs/sensor_status_msg.h>

#include <zynqtosros_msgs/controller_command.h>
#include <zynqtosros_msgs/bitflip_command_srv.h>
#include <zynqtosros_msgs/bitflip_status_msg.h>

#include <std_msgs/Empty.h>

#include <boost/thread/thread.hpp>

//#include <fstream>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

class zynqtosros_interface
{

public:
    zynqtosros_interface(ros::NodeHandle &n_);
    ~zynqtosros_interface();

    void servo_status_publisher();

private:
    ros::NodeHandle n;

    ros::Publisher servo_status_pub;
    ros::ServiceServer servo_command_srv;
    ros::ServiceServer controller_command_srv;

    ros::ServiceServer bitflip_command_srv;
    bool bitflip_command_srv_cb(zynqtosros_msgs::bitflip_command_srv::Request& req, zynqtosros_msgs::bitflip_command_srv::Response& res);
    /*int rate_bitflip;*/
    bool do_bitflip_runflag;
    void do_bitflip(int poll_rate, int servo_id);
    boost::thread* pthread_bitflip;
    ros::Publisher bitflip_status_pub;

    ros::Subscriber servo_command_sub;
    ros::Subscriber servo_command_arr_sub;

    bool servo_command_srv_cb(zynqtosros_msgs::servo_command::Request& req, zynqtosros_msgs::servo_command::Response& res);
    bool controller_command_srv_cb(zynqtosros_msgs::controller_command::Request& req, zynqtosros_msgs::controller_command::Response& res);

    void servo_command_msg_cb(const zynqtosros_msgs::servo_command_msg::ConstPtr &msg);
    void servo_command_msga_cb(const zynqtosros_msgs::servo_command_msga::ConstPtr &msga);

    ros::Publisher sensor_status_pub;
    ros::Subscriber sensor_command_sub;
    void sensor_command_msg_cb(const zynqtosros_msgs::sensor_command_msg::ConstPtr &msg);

    int zynq_fd;
    int zynq_bitflip_fd;

/*
    enum bare_metal_id_t : uint32_t
    {
        SERVO_1 = 0x1, SERVO_2, SERVO_3, SERVO_4, SERVO_5,
	    SERVO_6, SERVO_7, SERVO_8, SERVO_9, SERVO_10
    };

    enum bare_metal_key_t : uint32_t
    {
        POSITION    = 0x1,
    };
*/
    struct bare_metal_packet_t
    {
        uint32_t  id;
        uint32_t  reg_offset;
        uint32_t  value;
    };
};

#endif
