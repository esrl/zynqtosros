#!/bin/bash

sudo insmod zynqtos.ko regbase=0xffff8000

sudo mknod /dev/zynqtos c 244 0
sudo chgrp dialout /dev/zynqtos
sudo chmod g+rw /dev/zynqtos

sudo mknod /dev/zynqtos_bitflip c 244 1
sudo chgrp dialout /dev/zynqtos_bitflip
sudo chmod g+rw /dev/zynqtos_bitflip

sudo mknod /dev/zynqtos_softuart c 244 2
sudo chgrp dialout /dev/zynqtos_softuart
sudo chmod g+rw /dev/zynqtos_softuart
