#!/bin/bash
if [ "$#" -ne 2 ]; then
    echo "Wrong number of arguments"
    exit -1;
fi
echo "-----------"
echo "Setting test flag to 1"
#read -p "Press enter to continue"
sudo devmem2 0xffff8024 w 0x1
echo "-----------"
echo "Writing motor data to ID $2"
#read -p "Press enter to continue"
if [ $1 == 1 ]; then
    sudo devmem2 0xffff8010 w 0x"$2"d6d8
else
    sudo devmem2 0xffff8010 w 0x"$2"0000
fi
echo "-----------"
echo "Clear sync flag"
#read -p "Press enter to continue"
sudo devmem2 0xffff8020 w 0x0
#echo "-----------"
#echo "Request sensor reading"
#read -p "Press enter to continue"
#sudo devmem2 0xffff800c w 0xb
echo "-----------"
echo "Read test flag"
#read -p "Press enter to continue"
sudo devmem2 0xffff8028 w




