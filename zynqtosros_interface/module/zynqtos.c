#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/uaccess.h> /* for put_user */
#include <linux/slab.h> /* kmalloc() */
#include <linux/io.h>
#include <linux/ioport.h>

#include <asm/io.h>
#include <linux/io.h>
#include <linux/ioport.h>
#include <linux/errno.h>

MODULE_AUTHOR("andb@mmmi.sdu.dk");
MODULE_LICENSE("GPLv3");

/*
 * Prototypes
 */

int init_module(void);
void cleanup_module(void);
static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);

#define SUCCESS 0
#define DEVICE_NAME "zynqtos"
#define REG_BASE_ADDR_RANGE 0xfff
#define REG_OFFSET_SENSOR_RESPONSE 0x8
#define REG_OFFSET_SYNCFLAG 0x20
#define REG_OFFSET_BITFLIP 0x24
#define REG_OFFSET_BITFLIP_RESPONSE 0x28
#define SOFTUART_TX_FLAG_OFFSET 0x0
#define SOFTUART_TX_DATA_OFFSET 0x4
#define SOFTUART_BASE_ADDRESS 0xFFFF9000


#define MINOR_ZYNQTOS 0
#define MINOR_ZYNQTOS_BITFLIP 1
#define MINOR_ZYNQTOS_SOFTUART 2

#define BARE_METAL_PACKET_SIZE 12
struct bare_metal_packet_t
{
  uint32_t  id;
  uint32_t  reg_offset;
  uint32_t  value;
};

/*
 * Command line arguments
 */
void __iomem *base_addr=0;
static unsigned long regbase=0;
module_param(regbase, ulong, 0);

void __iomem *base_addr_bmc=0;
static unsigned long regbase_bmc=0;

void __iomem *base_addr_softuart=0;
static unsigned long regbase_softuart=0;

static int Major;
//static int Device_Open = 0;
static int Device_Open_by_minor[3]={0,0,0};

#define VMEM_BUFFER_SIZE 100
static void *vmem=0;
//static unsigned long pmem=0;

static struct file_operations fops = {
  .owner = THIS_MODULE,
  .read = device_read,
  .write = device_write,
  .open = device_open,
  .release = device_release
};


/*
 * This function is called when the module is loaded
 */
int init_module(void)
{

  //LDD3 "The old way". Depricated. could be removed soon...
  Major = register_chrdev(0, DEVICE_NAME, &fops);

  if (Major < 0) {
    printk(KERN_ALERT "Registering char device failed with %d\n", Major);
    return Major;
  }

  printk(KERN_INFO "Assigned major number %d.\n", Major);
  printk(KERN_INFO "Create device file with 'mknod /dev/%s c %d 0'.\n", DEVICE_NAME, Major);
  printk(KERN_INFO "Remove device file and module when done.\n");

  if(regbase)
  {
    //regbase=0xffff8000;
    printk(KERN_INFO "Argument regbase:0x%0lx\n",regbase);

    //if(!request_mem_region(regbase, BARE_METAL_PACKET_SIZE ,DEVICE_NAME))
    /*if(!request_mem_region(regbase, 0x14 ,DEVICE_NAME))
    {
      printk(KERN_INFO "Failed request_mem_region.\n");
	    return -EBUSY;
    }
    */

    /* Map memory mapped IO registers into virtual memory */
    //base_addr = ioremap_nocache(regbase, BARE_METAL_PACKET_SIZE);
    base_addr = ioremap_nocache(regbase, REG_BASE_ADDR_RANGE);
    if(!base_addr)
    {
      printk(KERN_INFO "Failed ioremap.\n");
	    return -EIO;
    }

    regbase_bmc = 0xfffffff0;
    base_addr_bmc = ioremap_nocache(regbase_bmc, 4);
    if(!base_addr_bmc)
    {
	     printk(KERN_INFO "Failed ioremap bare metal control.\n");
	     return -EIO;
    }
    else
    {
	     printk(KERN_INFO "Writing start-signal byte to bare metal control.\n");
	     iowrite32(0x18000000, base_addr_bmc);
    }

    regbase_softuart = SOFTUART_BASE_ADDRESS;
    base_addr_softuart = ioremap_nocache(regbase_softuart, 8);
    if(!base_addr_softuart)
    {
       printk(KERN_INFO "Failed ioremap soft uart.\n");
       return -EIO;
    }

    printk(KERN_INFO "Base address at virt mem 0x%p\n",base_addr);
    printk(KERN_INFO "Base address at phys mem 0x%0llx\n",(long long unsigned)virt_to_phys(base_addr));

  }
  else
  {
    printk(KERN_INFO "No base address argument. Allocating %i bytes of kernel memory for playing around\n", VMEM_BUFFER_SIZE);
    vmem = kmalloc(VMEM_BUFFER_SIZE, GFP_KERNEL);
    if(!vmem)
    {
        printk(KERN_ALERT "Failed to kmalloc()");
        return -EBUSY;
    }

    memset(vmem, 0, VMEM_BUFFER_SIZE);

    //pmem = virt_to_phys(vmem);
    printk(KERN_INFO "Allocated virt mem 0x%p\n",vmem);
    printk(KERN_INFO "Allocated phys mem 0x%0llx\n",(long long unsigned)virt_to_phys(vmem));
  }

  return SUCCESS;
}

/*
 * This function is called when the module is unloaded
 */
void cleanup_module(void)
{
  unregister_chrdev(Major, DEVICE_NAME);
  kfree(vmem);
  if(base_addr){
    iounmap(base_addr);
    release_mem_region(regbase, REG_BASE_ADDR_RANGE);
    iounmap(base_addr_bmc);
    release_mem_region(regbase_bmc, 4);
    iounmap(base_addr_softuart);
    release_mem_region(regbase_softuart, 8);
  }
}

/*
 * Called when a process tries to open the device file, like
 * "cat /dev/zynqtos"
 */
static int device_open(struct inode *inode, struct file *filp)
{
/*
  if (Device_Open)
    return -EBUSY;

  Device_Open++;
*/
  unsigned int minor = iminor(inode);

  if (Device_Open_by_minor[minor])
    return -EBUSY;

  Device_Open_by_minor[minor]++;

  printk(KERN_INFO "device_open: (minor:count) %i:%i %i:%i %i:%i",
   0, Device_Open_by_minor[0],
   1, Device_Open_by_minor[1],
   2, Device_Open_by_minor[2]
  );

  return SUCCESS;
}

/*
 * Called when a process closes the device file.
 */
static int device_release(struct inode *inode, struct file *filp)
{
  //Device_Open--;

  Device_Open_by_minor[iminor(inode)]--;

  printk(KERN_INFO "device_release: (minor:count) %i:%i %i:%i %i:%i",
   0, Device_Open_by_minor[0],
   1, Device_Open_by_minor[1],
   2, Device_Open_by_minor[2]
  );


  return SUCCESS;
}

/*
 * Called when a process, which already opened the dev file, attempts to read
 * from it.
 */
static ssize_t device_read(
			   struct file *filp, /* see include/linux/fs.h   */
			   char *buffer,      /* buffer to fill with data */
			   size_t length,     /* length of the buffer     */
			   loff_t *offset)
{
  int bytes_read = 0;
  char kernel_bytes[4];
  unsigned int val = 0;

  //printk(KERN_INFO "read: got buffer with length=%zd offset=%lld\n",length,*offset);

  if(regbase)
  {
    unsigned int minor = iminor(filp->f_path.dentry->d_inode);

    if (minor == MINOR_ZYNQTOS)
    {
      //printk(KERN_INFO "HW\n");
      //iowrite32(0xb,base_addr+0xC);//request sensor 11

      //memcpy_fromio(kernel_bytes, base_addr+0x8, BARE_METAL_PACKET_SIZE);// region
      val = ioread32(base_addr + REG_OFFSET_SENSOR_RESPONSE);
      memcpy(kernel_bytes, &val, 4);

      //printk(KERN_INFO "read: (%u) kernel_bytes: 0:0x%02x 1:0x%02x 2:0x%02x 3:0x%02x\n",
      //	   val,kernel_bytes[0], kernel_bytes[1], kernel_bytes[2], kernel_bytes[3]);

      while( bytes_read < 4 )
      {
        put_user(kernel_bytes[bytes_read], buffer+bytes_read);
        bytes_read++;
      }
      //ioread8_rep(base_addr,buffer,BARE_METAL_PACKET_SIZE);// same address
      //memcpy_fromio(buffer, base_addr, BARE_METAL_PACKET_SIZE);// region
    }
    else if (minor == MINOR_ZYNQTOS_BITFLIP)
    {
      //printk(KERN_INFO "read bitflip");
      val = ioread32(base_addr + REG_OFFSET_BITFLIP_RESPONSE);
      memcpy(kernel_bytes, &val, 4);

      while( bytes_read < 4 )
      {
        put_user(kernel_bytes[bytes_read], buffer+bytes_read);
        bytes_read++;
      }
    }
    else if (minor == MINOR_ZYNQTOS_SOFTUART)
    {
      //Is data present on softuart? Return if not.
      uint32_t tx_flag = ioread32(base_addr_softuart + SOFTUART_TX_FLAG_OFFSET);
      if(!tx_flag)
      {
        return 0;
      }
      else
      {
        //printk(KERN_INFO "Data present on softuart\n");
        //Else continue to read until 0x0A (newline)
        while(true)
        {
          uint32_t data = ioread32(base_addr_softuart + SOFTUART_TX_DATA_OFFSET);
          iowrite32(0, base_addr_softuart + SOFTUART_TX_FLAG_OFFSET);
          put_user((char)data, buffer+bytes_read);
          bytes_read++;
          if(data == 0x0A) break;
          while(!ioread32(base_addr_softuart + SOFTUART_TX_FLAG_OFFSET));
        }
      }
    }
    //House keeping, shared among minor numbers...
    length=0;
    //bytes_read=BARE_METAL_PACKET_SIZE;
    *offset+=bytes_read;

    return bytes_read;

  }
  else
  {
    unsigned int ma = imajor(filp->f_path.dentry->d_inode);
    unsigned int mi = iminor(filp->f_path.dentry->d_inode);
      printk(KERN_INFO "read: %i %i got buffer with length=%zd offset=%lld\n",ma,mi,length,*offset);

      if( length >= VMEM_BUFFER_SIZE )
      {
        printk(KERN_INFO "read: aborting - got buffer with length=%zd >= %i\n",length,VMEM_BUFFER_SIZE);
        return 0;
      }
      while( bytes_read < length )
      {
          put_user(*((char*)vmem + bytes_read++),buffer++);
      }

      //length=0;
      *offset += bytes_read;
      return bytes_read;
  }
}

static void stat_collect(uint32_t key)
{
    static uint32_t stat_key[16];
    static uint32_t stat_val[16];
    static uint32_t stat_count=0;
    uint32_t tmp;
    int i,j=0;

    if (stat_count++ > 1000)
    {
        printk(KERN_INFO "Servo ID statistic:");
        stat_count=0;
        //Sort (bubble) by key for better readable printout
        for(i=0; i<16; i++)
        {
            for(j=0; j<16; j++)
            {
                if(stat_key[j]>stat_key[i])
                {
                    tmp=stat_key[j];
                    stat_key[j]=stat_key[i];
                    stat_key[i]=tmp;
                    tmp=stat_val[j];
                    stat_val[j]=stat_val[i];
                    stat_val[i]=tmp;
                }
            }
        }
        //Print keys where value != 0
        for(i=0; i<16; i++)
        {
            if(stat_val[i]!=0)
            {
                printk(KERN_INFO " 0x%x:%u", stat_key[i], stat_val[i]);
                stat_key[i] = stat_val[i] = 0;
            }
        }
        printk(KERN_INFO "\n");
    }
    //Search for key and increment value
    for(i=0; i<16; i++)
    {
        if (stat_key[i]==key)
        {
            stat_val[i]++;
            return;
        }
    }
    //if key not found, create it and increment value
    for(i=0; i<16; i++)
    {
        if (stat_key[i]==0)
        {
            stat_key[i]=key;
            stat_val[i]++;
            return;
        }
    }

}

/*
 * Called when a process writes to dev file.
 */
static ssize_t device_write(
			    struct file *filp,
			    const char *buffer,
			    size_t length,
			    loff_t *offset)
{
  int bytes_written = 0;
  char user_bytes[sizeof(struct bare_metal_packet_t)];
  struct bare_metal_packet_t packet;
  //unsigned int val;
  uint32_t flag_timeout = 0;

  //printk(KERN_INFO "write: got buffer with length=%zd offset=%lld\n",length,*offset);

  if(regbase)
  {
    unsigned int minor = iminor(filp->f_path.dentry->d_inode);

    if (minor == MINOR_ZYNQTOS)
    {

      while( bytes_written < BARE_METAL_PACKET_SIZE )
      {
        get_user(user_bytes[bytes_written], buffer + bytes_written);
        bytes_written++;
      }

      memcpy(&packet, &user_bytes, sizeof(struct bare_metal_packet_t));

      if(packet.reg_offset < REG_BASE_ADDR_RANGE)
      {
        //if(packet.reg_offset == 0xC)
        //printk(KERN_INFO "write: got reg_offset:0xC with value 0x%02x\n",packet.value);

        //wait for bare metal core sync flag to go high (signals BM ready to reveive)
        //printk(KERN_INFO "waiting for sync flag (%u)\n",ioread32(base_addr + REG_OFFSET_SYNCFLAG));
        while(ioread32(base_addr + REG_OFFSET_SYNCFLAG) != 1)
        {
          if(++flag_timeout>0xfffffff)
          {
	          printk(KERN_INFO "Timed out \n");
	          return 0;
          }
        }
        //printk(KERN_INFO "Tsync %u\n",flag_timeout);
	//memcpy_toio(base_addr+0x10, user_bytes, BARE_METAL_PACKET_SIZE);// region
	iowrite32(packet.value, base_addr + packet.reg_offset);
	iowrite32(0, base_addr + REG_OFFSET_SYNCFLAG);
	stat_collect(packet.reg_offset);
      }
    }
    else if (minor == MINOR_ZYNQTOS_BITFLIP)
    {
      //printk(KERN_INFO "write bitflip");

      while( bytes_written < BARE_METAL_PACKET_SIZE )
      {
        get_user(user_bytes[bytes_written], buffer + bytes_written);
        bytes_written++;
      }

      memcpy(&packet, &user_bytes, sizeof(struct bare_metal_packet_t));

      //Clear bit flip response flag from previous iteration and set bit flip flag
      iowrite32(0, base_addr + REG_OFFSET_BITFLIP_RESPONSE);
      iowrite32(1, base_addr + REG_OFFSET_BITFLIP);

      //Wait for BM to signal ready to receive (sync flag goes high)
      while(ioread32(base_addr + REG_OFFSET_SYNCFLAG) != 1)
      {
        if(++flag_timeout>0xfffffff)
        {
          printk(KERN_INFO "Timed out \n");
          return 0;
        }
      }

      //write motor data
      iowrite32(packet.value, base_addr + packet.reg_offset);
      iowrite32(0, base_addr + REG_OFFSET_SYNCFLAG);
      stat_collect(packet.reg_offset);

/*
      //original
      while( bytes_written < 4 )
      {
        get_user(user_bytes[bytes_written], buffer + bytes_written);
        bytes_written++;
      }

      memcpy(&val, &user_bytes, 4);
      iowrite32(val, base_addr + REG_OFFSET_BITFLIP);
*/
    }

    //House keeping -shared acros minor numbers..
    length=0;
    *offset+=bytes_written;
    return bytes_written;

  }
  else
  {
    unsigned int ma = imajor(filp->f_path.dentry->d_inode);
    unsigned int mi = iminor(filp->f_path.dentry->d_inode);
    printk(KERN_INFO "write: %i %i got buffer with length=%zd offset=%lld\n",ma,mi,length,*offset);
    if( length >= VMEM_BUFFER_SIZE )
    {
      printk(KERN_INFO "write: aborting - got buffer with length=%zd >= %i\n",length,VMEM_BUFFER_SIZE);
      return 0;
    }
      while( bytes_written < length )
      {
          get_user(*((char*)vmem + bytes_written++),buffer++);
      }
      length=0;
      *offset += bytes_written;
      return bytes_written;
  }
}
