// Copyright 2018 Anders Bøgild (andb@mmmi.sdu.dk)
// Training Technology Lab
// SDU Health Informatics and Technology, The Maersk Mc-Kinney Moller Institute.

#include "zynqtosros_interface.hpp"

using namespace std;

zynqtosros_interface::zynqtosros_interface(ros::NodeHandle &n_):
    n(n_)
{

    zynq_fd = open("/dev/zynqtos", O_RDWR ); //open device file
    fcntl(zynq_fd, F_SETFL, 0); //reset all flags

    //zynq_bitflip_fd = open("/dev/zynqtos_bitflip", O_RDWR ); //open device file
    //fcntl(zynq_bitflip_fd, F_SETFL, 0); //reset all flags


    servo_status_pub = n.advertise<zynqtosros_msgs::servo_status>(
	n.param(string("servo_status_topic"), string("/zynqtosros_interface/servo_status")), 1);

    servo_command_srv = n.advertiseService(
	n.param(string("servo_command_service"), string("/zynqtosros_interface/servo_command")),
	&zynqtosros_interface::servo_command_srv_cb, this);

    controller_command_srv = n.advertiseService(
	n.param(string("controller_command_service"), string("/zynqtosros_interface/controller_command")),
	&zynqtosros_interface::controller_command_srv_cb, this);

    servo_command_sub = n.subscribe(n.param(string("servo_command_topic"), string("/zynqtosros_interface/servo_command_topic")), 1, &zynqtosros_interface::servo_command_msg_cb, this);
    servo_command_arr_sub = n.subscribe(n.param(string("servo_command_arr_topic"), string("/zynqtosros_interface/servo_command_arr_topic")), 1, &zynqtosros_interface::servo_command_msga_cb, this);

    sensor_command_sub = n.subscribe(n.param(string("sensor_command_topic"), string("/zynqtosros_interface/sensor_command_topic")), 1, &zynqtosros_interface::sensor_command_msg_cb, this);

    sensor_status_pub = n.advertise<zynqtosros_msgs::sensor_status_msg>(
	  n.param(string("sensor_status_topic"), string("/zynqtosros_interface/sensor_status_topic")), 1);

    bitflip_command_srv = n.advertiseService(
	     n.param(string("bitflip_command_service"), string("/zynqtosros_interface/bitflip_command")),
	      &zynqtosros_interface::bitflip_command_srv_cb, this);

    bitflip_status_pub = n.advertise<zynqtosros_msgs::bitflip_status_msg>(
        n.param(string("bitflip_status_topic"), string("/zynqtosros_interface/bitflip_status_topic")), 1);

}

zynqtosros_interface::~zynqtosros_interface()
{
    ROS_INFO("~zynqtosros_interface -closing files");
    close(zynq_fd);
    //close(zynq_bitflip_fd);
}

void zynqtosros_interface::servo_status_publisher()
{
  //Not used
}

bool zynqtosros_interface::servo_command_srv_cb(
    zynqtosros_msgs::servo_command::Request& req,
    zynqtosros_msgs::servo_command::Response& res)
{

    char* buffer = new char[sizeof(struct bare_metal_packet_t)];

    ROS_INFO("Servo Command Service: req.servo_id=%i .key=%i .value=%i",(uint32_t)req.servo_id,(uint32_t)req.key,(uint32_t)req.value);
    ROS_INFO("Buffer size: %ld", sizeof(*buffer));

    //Construct a packet with data and copy it to buffer
    struct bare_metal_packet_t wpacket;
    wpacket.id = req.servo_id;
    wpacket.reg_offset = req.key;
    wpacket.value = req.value;
    memcpy(buffer,&wpacket, sizeof(struct bare_metal_packet_t));

    //write(zynq_fd,buffer, sizeof(*buffer));
    write(zynq_fd,&buffer[8], 4);

    ROS_INFO("Write: id= 0x%02x 0x%02x 0x%02x 0x%02x key= 0x%02x 0x%02x 0x%02x 0x%02x value= 0x%02x 0x%02x 0x%02x 0x%02x",
	     buffer[0],buffer[1],buffer[2],buffer[3],
       buffer[4],buffer[5],buffer[6],buffer[7],
       buffer[8],buffer[9],buffer[10],buffer[11]);
    /*
    memset(buffer,0,sizeof(*buffer)); //clear buffer for testing purposes

    read(zynq_fd,buffer,sizeof(*buffer));
    struct bare_metal_packet_t rpacket;
    memcpy(&rpacket, buffer, sizeof(*buffer));

    ROS_INFO("Read: id= 0x%02x 0x%02x 0x%02x 0x%02x key= 0x%02x 0x%02x 0x%02x 0x%02x value= 0x%02x 0x%02x 0x%02x 0x%02x",
	     buffer[0],buffer[1],buffer[2],buffer[3],
       buffer[4],buffer[5],buffer[6],buffer[7],
       buffer[8],buffer[9],buffer[10],buffer[11]);


    ROS_INFO("Zynq response: id:%0x .key:%0x .value:%0x", (uint32_t)rpacket.id, (uint32_t)rpacket.key, (uint32_t)rpacket.value);

    */
    res.success = true;
    res.error = "";

    // Clean up, delete buffer
    delete buffer;

    return true;
}

void zynqtosros_interface::servo_command_msg_cb(const zynqtosros_msgs::servo_command_msg::ConstPtr &msg)
{

    char* buffer = new char[sizeof(struct bare_metal_packet_t)];

    //ROS_INFO("Servo Command Topic: req.servo_id=%i .key=%i .value=%i",(uint32_t)msg->servo_id,(uint32_t)msg->key,(uint32_t)msg->value);

    //Construct a packet with data from ROS message and copy it to buffer
    struct bare_metal_packet_t wpacket;
    wpacket.id = msg->servo_id;
    wpacket.reg_offset = msg->reg_offset;
    wpacket.value = msg->value;
    wpacket.value += (wpacket.id << 16); //currently bare metal core reads servo id as byte 3 in the 4 byte value field.
    memcpy(buffer,&wpacket, sizeof(struct bare_metal_packet_t));

    write(zynq_fd,buffer, sizeof(struct bare_metal_packet_t));

    delete buffer;

}

//Array version
void zynqtosros_interface::servo_command_msga_cb(const zynqtosros_msgs::servo_command_msga::ConstPtr &msga)
{

    char* buffer = new char[sizeof(struct bare_metal_packet_t)];

    //ROS_INFO("Servo Command Topic: req.servo_id=%i .key=%i .value=%i",(uint32_t)msg->servo_id,(uint32_t)msg->key,(uint32_t)msg->value);

    unsigned int i=0;
    while(i < msga->num_servos)
    {
      //Construct a packet with data from ROS msg, copy to buffer, write buffer to zynq
	    struct bare_metal_packet_t wpacket;
      wpacket.id = msga->servo_id[i];
      wpacket.reg_offset = msga->reg_offset[i];
      wpacket.value = msga->value[i];
      wpacket.value += (wpacket.id << 16); //currently bare metal core reads servo id as byte 3 in the 4 byte value field.
      memcpy(buffer,&wpacket, sizeof(struct bare_metal_packet_t));

      write(zynq_fd,buffer, sizeof(struct bare_metal_packet_t));
	    i++;
    }

    delete buffer;
}

void zynqtosros_interface::sensor_command_msg_cb(const zynqtosros_msgs::sensor_command_msg::ConstPtr &msg)
{

    char* buffer = new char[sizeof(struct bare_metal_packet_t)];

    //Construct a packet with data from ROS msg, copy to buffer, write buffer to zynq
    struct bare_metal_packet_t packet;
    packet.id = 0;//msg->sensor_id;
    packet.reg_offset = msg->reg_offset;
    packet.value = msg->sensor_id; //TODO: for now the zynqtos driver reads the sensor ID from the value field
    memcpy(buffer,&packet, sizeof(struct bare_metal_packet_t));
    write(zynq_fd,buffer, sizeof(struct bare_metal_packet_t));

    memset(buffer,0,sizeof(struct bare_metal_packet_t)); //clear buffer

    //Read from zynq to buffer, construct packet from buffer, publish packet as ROS message.
    read(zynq_fd,&buffer[8],4);
    memcpy(&packet,buffer, sizeof(struct bare_metal_packet_t));
    zynqtosros_msgs::sensor_status_msg msg_pub;
    msg_pub.sensor_id=packet.id;
    msg_pub.value=packet.value;
    sensor_status_pub.publish(msg_pub);

    delete buffer;
}


bool zynqtosros_interface::controller_command_srv_cb(
    zynqtosros_msgs::controller_command::Request& req,
    zynqtosros_msgs::controller_command::Response& res)
{

    ROS_INFO("Controller Command Service:");

    res.success = true;
    res.error = "";

    return true;
}

//This method is going to run in a seperate boost::thread
void zynqtosros_interface::do_bitflip(int poll_rate, int servo_id)
{
  //ros::NodeHandlePtr node = boost::make_shared<ros::NodeHandle>();
  //ros::Publisher pub_b = node->advertise<std_msgs::Empty>("bitflip", 10);

  int zynq_bitflip_fd = open("/dev/zynqtos_bitflip", O_RDWR ); //open device file
  fcntl(zynq_bitflip_fd, F_SETFL, 0); //reset all flags

  char* buffer = new char[sizeof(struct bare_metal_packet_t)];
  struct bare_metal_packet_t bfpacket;

  uint32_t flip_count = 0;
  uint32_t bit_flip_response = 0;

  //send count status 0
  zynqtosros_msgs::bitflip_status_msg msg;
  msg.count = flip_count;
  bitflip_status_pub.publish(msg);

  ros::Rate loop_rate(poll_rate);
  while( ros::ok() && do_bitflip_runflag )
  {
    //std_msgs::Empty msg;
    //pub_b.publish(msg);
    //uint32_t val = 1;
    //write(zynq_bitflip_fd,&val, 4);

    bfpacket.id = servo_id;
    bfpacket.reg_offset = 0x10;
    bfpacket.value = flip_count%2 ? 0 : 0xD6D8;
    bfpacket.value += (bfpacket.id << 16); //currently bare metal core reads servo id as byte 3 in the 4 byte value field.
    memcpy(buffer,&bfpacket, sizeof(struct bare_metal_packet_t));
    write(zynq_bitflip_fd, buffer, sizeof(struct bare_metal_packet_t));

    //Wait for bit flip Read from zynq
    while( (bit_flip_response == 0) && do_bitflip_runflag )
    {
        read(zynq_bitflip_fd,&bit_flip_response,4);
    }

    flip_count++;

    //report back
    if(!(flip_count % 1000))
    {
      zynqtosros_msgs::bitflip_status_msg msg;
      msg.count = flip_count;
      bitflip_status_pub.publish(msg);
    }
    //loop_rate.sleep();
  }

  //If execution reaches here, runflag has been set to false..
  //Clean up
  close(zynq_bitflip_fd);
  delete buffer;
}

bool zynqtosros_interface::bitflip_command_srv_cb(
    zynqtosros_msgs::bitflip_command_srv::Request& req,
    zynqtosros_msgs::bitflip_command_srv::Response& res)
{
    static bool enabled = false;

    enabled = !enabled; //Flip enabled state

    if(enabled)
    {
      int rate_bitflip = 1;
      int servo_id = req.servo_id;

      //spawn thread
      do_bitflip_runflag = true;
      pthread_bitflip = new boost::thread(boost::bind(&zynqtosros_interface::do_bitflip, this, rate_bitflip, servo_id));
    }
    else
    {
      //signal thread to stop and join
      do_bitflip_runflag = false;
      pthread_bitflip->join();
      delete pthread_bitflip;
    }


    ROS_INFO("Bitflip Command Service: %s", enabled?"ENABLED":"DISABLED");

    res.enabled = enabled; //report back new state

    return true;
}
