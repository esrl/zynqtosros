// Copyright 2018 Anders Bøgild (andb@mmmi.sdu.dk)
// Training Technology Lab
// SDU Health Informatics and Technology, The Maersk Mc-Kinney Moller Institute.

#include <ros/ros.h>
#include "zynqtosros_interface.hpp"

using namespace std;

int main(int argc, char **argv)
{
    /*Init ros node*/
    ros::init(argc, argv, "zynqtosros_interface");
    ros::NodeHandle n("~");

    /*Init interface*/
    zynqtosros_interface zynqtosros_interface_(n);

    ros::spin();
    /*
    while (ros::ok())
    {
	     zynqtosros_interface_.servo_status_publisher();
	     ros::spinOnce();
    }
    */
}
